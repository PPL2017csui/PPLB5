var db = require('../db');

// Make new request.
exports.add = function(sender, requestUpdate, callback) {
  var request = db.get().collection('DummyRequest');
  request.insertOne({requester:sender, requested:requestUpdate.target , message:requestUpdate.message }, function(err,result){
	callback(err, requestUpdate.message);
  });
}