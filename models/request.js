var db = require('../db');
var insert = require('./insertRequest');

// validation for new request.
exports.add = function(sender, requestUpdate, callback) {
	
  var request = db.get().collection('DummyRequest');
	
  request.findOne( {$or: [{$and: [ {'requester': requestUpdate.target } , {'requested': sender } ]  } , {$and: [ {'requester': sender } , {'requested': requestUpdate.target } ] }]}, function (err, result){
	if (!result){
	  insert.add(sender, requestUpdate, callback);	
	} else {
	  callback(err, "Request have been created");
	}
  });
	
};
