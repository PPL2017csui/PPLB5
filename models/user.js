var db = require('../db')

// get user where his_username = username
exports.get = function(username, callback) {
    var profile = db.get().collection('DummyProfile2');
    profile.findOne({"username":username}, function (err, result) {
        if(result) {
            callback(err,{user: result});
        } else {
            callback(1,"not found");
        }
    });
}

// update user profile
exports.update = function(username, userUpdate, callback) {
    var setter = {
        email:userUpdate.email,
        phone: userUpdate.phone,
        location: userUpdate.location,
        skill: userUpdate.skill,
        achievement: userUpdate.achievement,
        interest: userUpdate.interest,
        facebook: userUpdate.facebook,
        twitter: userUpdate.twitter,
        linkein: userUpdate.linkein
    }
    if(userUpdate.photo) {
        setter.photo = userUpdate.photo;
    }
    if(userUpdate.status) {
        setter.status = parseInt(userUpdate.status) + 1;
    }
    var profile = db.get().collection('DummyProfile2');
    profile.update({username:username}, {$set: setter}, function(err,result){
        callback(err);
    });
}

// get users with their status for profile page
exports.getWithStatus = function(username, callback) {
    var profile = db.get().collection('DummyProfile2');
    profile.aggregate([
        {$match: {username: username}},
        {$lookup: {from: 'DummyStatus' ,localField: 'status', foreignField: 'id', as:'status' }}
    ], function(err, items) {
        var ret = {};
        if(items.length > 0){
            ret = {
                user: items[0],
                status: items[0].status[0].status
            }
        } else {
            err = new Error("User not found");
        }
        callback(err,ret);
    });
}

exports.getNewFriends = function(interest, faculty, username, callback) {
    var profile = db.get().collection('DummyProfile2');
    if(interest == "All" && faculty == "All") {
        profile.find().toArray(function (err, result) {
            if(err) {
                callback(1, "not found");
            } else {
                var users_id = [];
                for(var i = 0; i < result.length; i++) {
                    users_id.push(result[i].id);
                }
                profile.findOne({"username" : username}, function(err, result) {
                    if(result) {
                        var friend_arr = result.friend_id;
                        friend_arr.push(result.id);
                        profile.aggregate([
                            {$match: {$and : [{id: {$in: users_id}}, {id: {$nin: friend_arr}}]}},
                            {$lookup: {from: 'DummyStatus', localField: 'status', foreignField: 'id', as: 'status'}}
                        ], function(err, items) {
                            callback(err, {user: items});
                        });
                    } else {
                        callback(1, "not found");
                    }
                });
            }
        });
    } else if(interest == "All" && faculty != "All") {
        profile.find({"faculty" : faculty}).toArray(function (err, result) {
            if(err) {
                callback(1, "not found");
            } else {
                var users_id = [];
                for(var i = 0; i < result.length; i++) {
                    users_id.push(result[i].id);
                }
                profile.findOne({"username" : username}, function(err, result) {
                    if(result) {
                        var friend_arr = result.friend_id;
                        friend_arr.push(result.id);
                        profile.aggregate([
                            {$match: {$and : [{id: {$in: users_id}}, {id: {$nin: friend_arr}}]}},
                            {$lookup: {from: 'DummyStatus', localField: 'status', foreignField: 'id', as: 'status'}}
                        ], function(err, items) {
                            callback(err, {user: items});
                        });
                    } else {
                        callback(1, "not found");
                    }
                });
            }
        });
    } else if(interest != "All" && faculty == "All") {
        profile.find({"interest" : interest}).toArray(function (err, result) {
            if(err) {
                callback(1, "not found");
            } else {
                var users_id = [];
                for(var i = 0; i < result.length; i++) {
                    users_id.push(result[i].id);
                }
                profile.findOne({"username" : username}, function(err, result) {
                    if(result) {
                        var friend_arr = result.friend_id;
                        friend_arr.push(result.id);
                        profile.aggregate([
                            {$match: {$and : [{id: {$in: users_id}}, {id: {$nin: friend_arr}}]}},
                            {$lookup: {from: 'DummyStatus', localField: 'status', foreignField: 'id', as: 'status'}}
                        ], function(err, items) {
                            callback(err, {user: items});
                        });
                    } else {
                        callback(1, "not found");
                    }
                });
            }
        });
    } else {
        profile.find({$and:[{"interest" : interest}, {"faculty" : faculty}]}).toArray(function (err, result) {
            if(err) {
             callback(1,"not found");
            } else {
                var users_id = [];
                for(var i = 0; i<result.length; i++){
                    users_id.push(result[i].id);
                }
                profile.findOne({"username":username}, function (err, result) {
                    if(result) {
                        var friend_arr = result.friend_id;
                        friend_arr.push(result.id);
                            profile.aggregate([
                                {$match: {$and : [{id:  { $in: users_id} },{id: { $nin: friend_arr}}]}},
                                {$lookup: {from: 'DummyStatus' ,localField: 'status', foreignField: 'id', as:'status' }}
                            ], function(err, items) {
                                callback(err,{user: items});
                        });
                    } else {
                        callback(1,"not found");
                    }
                });
            }
        });
    }
}

