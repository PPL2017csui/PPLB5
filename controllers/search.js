var faculty = require("../models/faculty");
var interest = require("../models/interest");
var user = require("../models/user");
var request = require("../models/request");

exports.getAllFormData = function(req, res, errors) {

  faculty.getAll(function(err, obj){
  	if(err){
  		res.send("404");
  	}

  	var faculty_arr = obj;


  	interest.getAll(function(err, obj){
  		if(err){
  		res.send("404");
  		}

	  	var interest_arr =obj

	  	res.render("search",{
      title: "Add Friends",
      faculty_arr: faculty_arr,
	  	interest_arr: interest_arr,
      activeUser: req.session.username,
      user: []
    });
	});

  });
 }

 exports.searchFriends = function(req,res){
   faculty.getAll(function(err, obj){
   	if(err){
   		res.send("404");
   	}

   	var faculty_arr = obj;


   	interest.getAll(function(err, obj){
   	  if(err){
   	    res.send("404");
   	  }
      var interest_arr = obj;

      user.getNewFriends(req.body.interest, req.body.faculty, req.session.username, function(err, obj){
        if(obj.user.length > 10){
          var result = [];
          for(var i = 0;i<10; i++){
            var randNum = Math.floor(Math.random()*obj.user.length);
            var user = obj.user[randNum];
            result[i] = user;
            obj.user.splice(randNum,1);
          }
          res.render("search",{
            title: "Add Friends",
            user: result,
            faculty_arr : faculty_arr,
            interest_arr : interest_arr,
            activeUser: req.session.username
          });
        } else {
        res.render("search",{
          title: "Add Friends",
          user: obj.user,
          faculty_arr : faculty_arr,
          interest_arr : interest_arr,
          activeUser: req.session.username
        });
      }
      });
    });
  });
 }

 
 
 
 // Insert request
exports.updateRequest = function(req,res) {
  if (req.body.message == ""){
	req.body.message = "Hello! My name is " + req.body.sender + ". I want to be friend with " + req.body.target;
  }
	
  request.add(req.body.sender, req.body, function(err, result) {
	res.redirect("/search");
  });
  
}