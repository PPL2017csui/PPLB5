$(document).on('click','.deleteButton', function() {
    $(this).parent().remove();    
});
function addInterest() {
    var value = $( "#interest" ).val();
    $( "#listInterest" ).append( '<li class="list-group-item">'+value+'<button type="button" class="btn btn-danger pull-right deleteButton"><i class="fa fa-times" aria-hidden="true"></i></button></li>' );
}
function addSkill() {
    var value = $( "#skill" ).val();
    $( "#listSkill" ).append( '<li class="list-group-item">'+value+'<button type="button" class="btn btn-danger pull-right deleteButton"><i class="fa fa-times" aria-hidden="true"></i></button></li>' );
}
function addAchievement() {
    var value = $( "#achievement" ).val();
    $( "#listAchievement" ).append( '<li class="list-group-item">'+value+'<button type="button" class="btn btn-danger pull-right deleteButton"><i class="fa fa-times" aria-hidden="true"></i></button></li>' );
}