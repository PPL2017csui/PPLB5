var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;
var should = chai.should();
var sinon = require('sinon');

var auth = require('../../middlewares/auth');

describe('middlewares/auth.js', function () {
    
    describe('#signed()', function() {

        it('should pass if user access his user page', function(){
            var req = {
                session: {
                    authenticated: true,
                    username: 'azmi41'
                },
                params: {
                    username: 'azmi41'
                }
            };
            var res = {
                status: sinon.spy(),
                send: sinon.spy()
            };
            var next = sinon.spy();
            auth.signed(req, res, next);
            next.calledOnce.should.be.true;
            res.status.calledOnce.should.be.false;
            res.send.calledOnce.should.be.false;
        });

        it('should redirect if user access others user page', function() {
            var req = {
                session: {
                    authenticated: true,
                    username: 'Carl.Brown'
                },
                params: {
                    username: 'azmi41'
                }
            };
            var res = {
                status: sinon.spy(),
                send: sinon.spy()
            };
            var next = sinon.spy();
            auth.signed(req, res, next);
            var args = null;
            if(res.status.args[0] && res.status.args[0][0]){
                args = res.status.args[0][0];
            }
            expect(args).equals(403);
            next.calledOnce.should.be.false;
            res.status.calledOnce.should.be.true;
            res.send.calledOnce.should.be.true;
        }); 

        it('should redirect if unsigned user access others user page', function(){
            var req = {
                session: {},
                params: {
                    username: 'azmi41'
                }
            };
            var res = {
                status: sinon.spy(),
                send: sinon.spy()
            };
            var next = sinon.spy();
            auth.signed(req, res, next);
            var args = null;
            if(res.status.args[0] && res.status.args[0][0]){
                args = res.status.args[0][0];
            }
            expect(args).equals(403);
            next.calledOnce.should.be.false;
            res.status.calledOnce.should.be.true;
            res.send.calledOnce.should.be.true;
        });

    });

    describe('#valid', function(){
        it('should valid',function(){
            var req, res ,next;

            req = {
                session : {
                    authenticated: true
                }
            };

            next = sinon.spy();

            auth.valid(req,res,next);
            next.calledOnce.should.be.true;

        });

        it('should forbidden', function(){
            var req, res, next;
            req = {
                session : {
                    authenticated: false
                }
            };

            res = {
                status: sinon.spy(),
                send: sinon.spy()
            }

            auth.valid(req,res,next);

            res.status.withArgs(403).calledOnce.should.be.true;
            res.send.withArgs('403: forbidden').calledOnce.should.be.true;


        });


    })
    
});