var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
var sinon = require('sinon');
var expect = require('chai').expect;
var interest = require("../models/interest");
var facultyModel = require('../models/faculty');
var searchControl = require('../controllers/search');
var user = require('../models/user');
var requestmdl = require('../models/request.js');
var db = require('../db');


describe('Search new Friend', () => {
	var faculty;
	var req = {};
	var res = {};
	var callback;
	var callbackSpy = sinon.spy(callback);
	var faculty_arr = {"id": 5, "facName": "Fakultas Ilmu Komputer"};
	var interest_arr = {"id": 1, "interest": "Programming"};

  var details, request;
  
  beforeEach(function() {
    details = {
      findOne: function(){
      }

    };
    request = {
      collection : function(myDb){
        return details;
      }
    };
    callbackSpy = sinon.spy(callback);
  });
  
  
	
  describe("Get Form Data from Database", () => {
    it('should export a function', () => {
      expect(searchControl.getAllFormData).to.be.a('function')
    })
  });

  after(function () {
    db.get.restore();
  });
  
  it('should render the search new friend', function(){
		var req, res, spy;
		req = res = {};
		spy = res.send = sinon.spy();
		renderSpy = res.render = sinon.spy();
		var obj = {
		  user : {
		    username : "Carl"
		  }
		};
		var req = {
			session : {
				username: "agni"
			}
		}
		var interestStub = sinon.stub(interest,'getAll').yields(true,interest_arr);
		var facultyStub = sinon.stub(facultyModel,'getAll').yields(true,faculty_arr);
		searchControl.getAllFormData(req, res, null);
		this.timeout(5000);
		spy.withArgs("404").calledTwice.should.be.true;
		renderSpy.withArgs("search",{ title: "Add Friends", faculty_arr: faculty_arr,	interest_arr: interest_arr, activeUser: req.session.username, user: []}).called.should.be.true;

		interest.getAll.restore();
		facultyModel.getAll.restore();

	});

	it('should render the search new friend and not sending 404 error if the err is null', function(){
		var req, res, spy;
		req = res = {};
		spy = res.send = sinon.spy();
		renderSpy = res.render = sinon.spy();
		var obj = {
		  user : {
		    username : "Carl"
		  }
		};
		var req = {
			session : {
				username: "agni"
			}
		}
		var interestStub = sinon.stub(interest,'getAll').yields(false,interest_arr);
		var facultyStub = sinon.stub(facultyModel,'getAll').yields(false,faculty_arr);
		searchControl.getAllFormData(req, res, null);
		this.timeout(5000);
		spy.neverCalledWith("404").should.be.true;
		renderSpy.withArgs("search",{ title: "Add Friends", faculty_arr: faculty_arr,	interest_arr: interest_arr, activeUser: req.session.username, user: []}).called.should.be.true;

		interest.getAll.restore();
		facultyModel.getAll.restore();

	});

	it('should return all the users who have the interest and faculty that user want and call err twice if the error is happening',function(){
		var req, res, spy;
		req = res = {};
		req = {
			body : {
				interest : "Programming",
				faculty : "Fakultas Ilmu Komputer"
			},
			session : {
				username: "Carl"
			}
		};
		spy = res.send = sinon.spy();
		renderSpy = res.render = sinon.spy();
		var obj = {
		  user : {
		    username : "Carl"
		  }
		};
		var interestStub = sinon.stub(interest,'getAll').yields(true,interest_arr);
		var facultyStub = sinon.stub(facultyModel,'getAll').yields(true,faculty_arr);
		var userStub = sinon.stub(user,'getNewFriends').yields(true,obj,"Dicky.Edgardo");
		searchControl.searchFriends(req,res);
		spy.withArgs("404").calledTwice.should.be.true;
		renderSpy.withArgs("search",{ title: "Add Friends", user:obj.user, faculty_arr: faculty_arr,	interest_arr: interest_arr, activeUser: req.session.username}).called.should.be.true;

		user.getNewFriends.restore();
		interest.getAll.restore();
		facultyModel.getAll.restore();
	});

	it('should return all the users who have the interest and faculty that user want and call err twice if the error is happening',function(){
		var req, res, spy;
		req = res = {};
		req = {
			body : {
				interest : "Programming",
				faculty : "Fakultas Ilmu Komputer"
			},
			session : {
				username: "Carl"
			}
		};
		spy = res.send = sinon.spy();
		renderSpy = res.render = sinon.spy();
		var obj = {
		  user : [1,2,3,4,5,6,7,8,9,10,11]
		};

		var faculty_arr = {"id": 5, "facName": "Fakultas Ilmu Komputer"}
		var interest_arr = {"id": 1, "interest": "Programming"};
		var interestStub = sinon.stub(interest,'getAll').yields(true,interest_arr);
		var facultyStub = sinon.stub(facultyModel,'getAll').yields(true,faculty_arr);
		var userStub = sinon.stub(user,'getNewFriends').yields(true,obj);
		searchControl.searchFriends(req,res);
		spy.withArgs("404").calledTwice.should.be.true;
		renderSpy.calledOnce.should.be.true;

		user.getNewFriends.restore();
		interest.getAll.restore();
		facultyModel.getAll.restore();
	});

	it('should not call res.send if the err is not happening',function(){
		var req, res, spy;
		req = res = {};
		req = {
			body : {
				interest : "Programming",
				faculty : "Fakultas Ilmu Komputer"
			},
			session : {
				username: "Carl"
			}
		};
		spy = res.send = sinon.spy();
		renderSpy = res.render = sinon.spy();
		var obj = {
		  user : {
		    username : "Carl"
		  }
		};

		var interestStub = sinon.stub(interest,'getAll').yields(false,interest_arr);
		var facultyStub = sinon.stub(facultyModel,'getAll').yields(false,faculty_arr);
		var userStub = sinon.stub(user,'getNewFriends').yields(true,obj,"Dicky.Edgardo");
		searchControl.searchFriends(req,res);
		spy.neverCalledWith("404").should.be.true;
		renderSpy.withArgs("search",{ title: "Add Friends", user:obj.user, faculty_arr: faculty_arr,	interest_arr: interest_arr, activeUser: req.session.username}).called.should.be.true;

		user.getNewFriends.restore();
		interest.getAll.restore();
		facultyModel.getAll.restore();
	});

	// it('should return callback not found', function() {
	// 	var interestStub = sinon.stub(interest,'getAll').yields(true,interest_arr);
	// 	var facultyStub = sinon.stub(facultyModel,'getAll').yields(true,faculty_arr);
	// 	var callback = sinon.stub().returns('err');
	// 	var test = user.getNewFriends(interestStub, facultyStub, callback);
	// 	assert.equals(test, "not found");
	// });
	
	
  it('should using template message when message is unfilled', function(){
	db.get.restore();
	sinon.stub(db,'get').returns(request);
	var req_update = {
	  params : {
	    username: "Julie"
	  }, 
	  body : {
	    sender: "Julie",
	    target: "Carl",
	    message: ""
	}};
		
	searchControl.updateRequest(req_update, callbackSpy);
		
	assert.equal(req_update.body.message, 'Hello! My name is Julie. I want to be friend with Carl')
		
  });
	
  it('should add profile', function () {
	var req, res, spy;
    req = res = {};
	
	var req_update = {
	  params : {
		username: "Julie"
	  }, 
	  body : {
	    sender: "Julie",
		target: "Carl",
		message: "Helo"
	}};
	
    var result = {
      isEmpty: function(){
        return true
      },
      array: function(){
        return true
      }
    }
    var resSpy = res.redirect = sinon.spy();
    sinon.stub(requestmdl,'add').yields("Carl",result);
    searchControl.updateRequest(req_update, res);
    resSpy.withArgs("/search").calledOnce.should.be.true;
    requestmdl.add.restore();
	

  });


});
