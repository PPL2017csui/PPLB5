var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
var sinon = require('sinon');

var friendList = require('../../controllers/friendList')
var friendjs = require('../../models/friend')
var db = require('../../db');

describe('controllers/friendList.js', function(){

  describe('#viewFriendList()', function(){
    var details, profile, callback, callbackSpy, result, err, username, req, items;

    beforeEach(function(){
      result = { friend_id: [1]};
      username = {"username": "agni"};
      req = { params: username };

      details = {
        findOne : function(err,result){
          return result;
        },
        aggregate : function(err,items){
          return items
        }
      };
      profile = {
        collection : function(myDb){
          return details;
        },
      };
      callbackSpy = sinon.spy(callback);
    });

    it('should render friendList',function(){
      var req = {params: {username: "Carl"}};
      var res = {};
      var err;
      var spy;
      spy = res.render = sinon.spy();
      var obj = { friends: "Carl"};
      var getStub = sinon.stub(friendjs,'get').yields(false,obj);
      var countStub = sinon.stub(friendjs,'getTotalRequest').yields(false,3);
      friendList.viewFriendList(req,res);
      spy.withArgs("friendList",{user: req.params.username, notif : 3, title:"Friends List", friends: obj.friends }).calledOnce.should.be.true;
      friendjs.get.restore();
      friendjs.getTotalRequest.restore();
    });

    it('should not render friendList',function(){
      var req = {params: {username: "Carl"}};
      var res = {};
      var err;
      var spy;
      res.status = function (){};    
      spy = res.send = sinon.spy();
      var getStub = sinon.stub(friendjs,'get').yields(true,"Carl");
      var countStub = sinon.stub(friendjs,'getTotalRequest').yields(true,3);
      friendList.viewFriendList(req,res);
      spy.withArgs("404: Page not found").calledOnce.should.be.true;
      friendjs.get.restore();
      friendjs.getTotalRequest.restore();
    });

    it('should not render empty friendList',function(){
      var req = {params: {username: "Carl"}};
      var res = {};
      var err;
      var spy;
      spy = res.render = sinon.spy();
      res.status = function (){};   
      var getStub = sinon.stub(friendjs,'get').yields(null,"empty");
      var countStub = sinon.stub(friendjs,'getTotalRequest').yields(false,3);
      friendList.viewFriendList(req,res);
      spy.withArgs("friendList", {user: req.params.username, notif : 3, title:"Friends List", friends: [] }).calledOnce.should.be.true;
      friendjs.get.restore();
      friendjs.getTotalRequest.restore();
    });
  });

  describe('#viewFriendRequest()', function(){
    var req, res;
    beforeEach(function(){
      req = {
        params: {
          username: 'azmi41'
        }
      };
      res = {
        render: sinon.spy(),
        status: sinon.spy(),
        send: sinon.spy()
      };
    });
  
    it('should render if there is no error', function(){
      var getRequestStub = sinon.stub(friendjs,'getRequest').yields(false,{});
      var countStub = sinon.stub(friendjs,'getTotalRequest').yields(false,3);
      friendList.viewFriendRequest(req,res);
      res.render.calledOnce.should.be.true;
      friendjs.getRequest.restore();
      friendjs.getTotalRequest.restore();
    });

    it('should send error if models count friend request error', function(){
      var getRequestStub = sinon.stub(friendjs,'getRequest').yields(true,null);
      var countStub = sinon.stub(friendjs,'getTotalRequest').yields(true,3);
      friendList.viewFriendRequest(req,res);
      res.status.calledOnce.should.be.true;
      res.send.calledOnce.should.be.true;
      friendjs.getRequest.restore();
      friendjs.getTotalRequest.restore();
    });

     it('should send error if models view friend request error', function(){
      var getRequestStub = sinon.stub(friendjs,'getRequest').yields(true,null);
      var countStub = sinon.stub(friendjs,'getTotalRequest').yields(false,3);
      friendList.viewFriendRequest(req,res);
      res.status.calledOnce.should.be.true;
      res.send.calledOnce.should.be.true;
      friendjs.getRequest.restore();
      friendjs.getTotalRequest.restore();
    });
  });
  
  describe('#acceptFriendRequest()', function(){
    var req, res;
    beforeEach(function(){
      req = {
        params: {
          username: 'azmi41',
          other: 'akmal61'
        }
      };
      res = {
        redirect: sinon.spy(),
        status: sinon.spy(),
        send: sinon.spy()
      };
    });
  
    it('should send error if deleteFriendRequest fail', function(){
      var deleteFriendRequestStub = sinon.stub(friendjs,'deleteFriendRequest').yields(true);
      var addToFriendStub = sinon.stub(friendjs,'addToFriend').yields(false);
      friendList.acceptFriendRequest(req,res);
      res.status.called.should.be.true;
      res.send.called.should.be.true;
      friendjs.deleteFriendRequest.restore();
      friendjs.addToFriend.restore();
    });

    it('should send error if addToFriend fail', function(){
      var deleteFriendRequestStub = sinon.stub(friendjs,'deleteFriendRequest').yields(false);
      var addToFriendStub = sinon.stub(friendjs,'addToFriend').yields(true);
      friendList.acceptFriendRequest(req,res);
      res.status.called.should.be.true;
      res.send.called.should.be.true;
      friendjs.deleteFriendRequest.restore();
      friendjs.addToFriend.restore();
    });

    it('should redirect if there is no error', function(){
      var deleteFriendRequestStub = sinon.stub(friendjs,'deleteFriendRequest').yields(true);
      var addToFriendStub = sinon.stub(friendjs,'addToFriend').yields(true);
      friendList.acceptFriendRequest(req,res);
      res.redirect.withArgs('/profile/azmi41/request').calledOnce.should.be.true;
      friendjs.deleteFriendRequest.restore();
      friendjs.addToFriend.restore();
    });
  });

  describe('#declineFriendRequest()', function(){
     var req, res;
    beforeEach(function(){
      req = {
        params: {
          username: 'azmi41'
        }
      };
      res = {
        redirect: sinon.spy(),
        status: sinon.spy(),
        send: sinon.spy()
      };
    });
  
    it('should send error if addToFriend fail', function(){
      var deleteFriendRequestStub = sinon.stub(friendjs,'deleteFriendRequest').yields(true);
      friendList.declineFriendRequest(req,res);
      res.status.called.should.be.true;
      res.send.called.should.be.true;
      friendjs.deleteFriendRequest.restore();
    });

    it('should redirect if there is no error', function(){
      var deleteFriendRequestStub = sinon.stub(friendjs,'deleteFriendRequest').yields(false);
      friendList.declineFriendRequest(req,res);
      res.redirect.withArgs('/profile/azmi41/request').calledOnce.should.be.true;
      friendjs.deleteFriendRequest.restore();
    });
  });
});


