var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
var sinon = require('sinon');
var Datauri = require('datauri');

var profilejs = require('../../controllers/profile');
var user = require('../../models/user.js');
var interest = require('../../models/interest.js');
var status = require('../../models/status.js');
var image = require("../../helpers/image");
var array = require("../../helpers/array");

describe('controllers/profile.js', function(){
  it('should send the obj when err is not null on the View Profile', function(){
    var req, res, spy;
    req = res = {};
    spy = res.send = sinon.spy();
    statusSpy = res.status = sinon.spy();
    req = {
      params : {
        username : "Carl"
      }
    };
    sinon.stub(user,'getWithStatus').yields(true,"Carl");
    profilejs.viewProfile(req, res);
    spy.withArgs("Carl").calledOnce.should.be.false;
    statusSpy.withArgs(404).calledOnce.should.be.true;
    user.getWithStatus.restore();
  });

  it('create object when the err is null on the View Profile', function(){
    var req, res, spy;
    req = res = {};
    spy = res.render = sinon.spy();
    req = {
      params : {
        username : "Carl"
      },
      session : {
        username : "Carl",
        authenticated : true
      }
    };
    var obj = {
      user : "Carl",
      status : "Available"
    }
    var myObj = {
      title : "Profile",
      activeUser: req.params.username,
      user : obj.user,
      status : obj.status,
      auth: true,
      isValid: req.session.authenticated
    };
    var findStub = sinon.stub(user,'getWithStatus').yields(null,obj);
    profilejs.viewProfile(req, res);
    spy.withArgs("profile", myObj).calledOnce.should.be.true;
    user.getWithStatus.restore();
  });

  it('should send the obj when err is not null on the View edit profile page', function(){
    var req, res, spy;
    req = res = {};
    spy = res.send = sinon.spy();
    req = {
      params : {
        username : "Carl"
      }
    };
    var findStub = sinon.stub(interest,'getAll').yields(true,"Carl");
    var statusStub = sinon.stub(status,'getAll').yields(true,"Carl");
    var userStub = sinon.stub(user,'get').yields(true,"Carl");
    profilejs.viewEditProfile(req, res, "errors");
    spy.withArgs(true).calledThrice.should.be.true;
    interest.getAll.restore();
    status.getAll.restore();
    user.get.restore();
  });

  it('should render the view edit profile', function(){
    var req, res, spy;
    req = res = {};
    spy = res.send = sinon.spy();
    renderSpy = res.render = sinon.spy();
    req = {
      params : {
        username : "Carl"
      }
    };
    var obj = {
      user : {
        username : "Carl"
      }
    };
    var users = {

      title:"Profile",
      username: "Carl",
      user: obj.user,
      errors: "errors",
      status_arr: "Carl",
      interest_arr: "Carl"
    };
    var findStub = sinon.stub(interest,'getAll').yields(true,"Carl");
    var statusStub = sinon.stub(status,'getAll').yields(true,"Carl");
    var userStub = sinon.stub(user,'get').yields(null,obj);
    profilejs.viewEditProfile(req, res, "errors");
    spy.withArgs(true).calledTwice.should.be.true;

    renderSpy.withArgs("editProfile",users).calledOnce.should.be.true;
    interest.getAll.restore();
    status.getAll.restore();
    user.get.restore();
  });

  it('should render the view edit profile', function(){
    var req, res, spy;
    req = res = {};
    spy = res.send = sinon.spy();
    renderSpy = res.render = sinon.spy();
    req = {
      params : {
        username : "Carl"
      }
    };
    var obj = {
      user : {
        username : "Carl"
      }
    };
    var users = {

      title:"Profile",
      username: "Carl",
      user: obj.user,
      errors: "errors",
      status_arr: "Carl",
      interest_arr: "Carl"
    };
    var findStub = sinon.stub(interest,'getAll').yields(false,"Carl");
    var statusStub = sinon.stub(status,'getAll').yields(false,"Carl");
    var userStub = sinon.stub(user,'get').yields(null,obj);
    profilejs.viewEditProfile(req, res, "errors");
    spy.neverCalledWith(false).should.be.true;

    renderSpy.withArgs("editProfile",users).calledOnce.should.be.true;
    interest.getAll.restore();
    status.getAll.restore();
    user.get.restore();
  });

  it('should display the error when uploaded image is not an actual image on update the profile', function(){
    var req, res, spy;
    req = res = {};
    req = {
      params : {
        username : "Carl"
      },
      file : {
        buffer : "mybuff"
      },
      assert : function(param1,param2){},
      getValidationResult : function(){}
    }
    var myObject = {
      notEmpty: function(){
        return true;
      },
      isNumeric: function(){
        return true;
      },
      isLink: function(param){
        return true;
      }
    };
    var nextObj = {
      then: function(param1){}
    };
    var result = {
      isEmpty: function(){
        return false
      },
      array: function(){
        return true
      }
    }
    var Object1 = {
      push : function(param1){
      }
    };
    sinon.stub(req,'assert').returns(myObject);
    sinon.stub(req,'getValidationResult').returns(nextObj);
    sinon.stub(result,'array').returns(Object1);
    var pushSpy = Object1.push = sinon.spy();
    var viewSpy = profilejs.viewEditProfile = sinon.spy();
    sinon.stub(nextObj,'then').yields(result);
    profilejs.updateProfile(req, res);
    pushSpy.withArgs({param:"photo", msg:"file photo harus image", value:null}).calledOnce.should.be.true;
    viewSpy.withArgs(req, res, result.array()).calledOnce.should.be.true;
    req.assert.restore();
    req.getValidationResult.restore();
    result.array.restore();
    nextObj.then.restore();
  });

  it('should update the profile', function(){
    var req, res, spy;
    req = res = {};
    req = {
      params : {
        username : "Carl"
      },
      file : {
        buffer : "mybuff",
        mimetype : "image/jpeg"
      },
      body : {
        skill : "java",
        achievement : "juara gemastik",
        photo : "dummy",
        interest : "programing"
      },
      assert : function(param1,param2){},
      getValidationResult : function(){}
    }
    var myObject = {
      notEmpty: function(){
        return true;
      },
      isNumeric: function(){
        return true;
      },
      isLink: function(param){
        return true;
      }
    };
    var nextObj = {
      then: function(param1){}
    };
    var result = {
      isEmpty: function(){
        return true
      },
      array: function(){
        return true
      }
    }
    var Object1 = {
      push : function(param1){
      }
    };
    sinon.stub(req,'assert').returns(myObject);
    sinon.stub(req,'getValidationResult').returns(nextObj);
    var resSpy = res.redirect = sinon.spy();
    sinon.stub(nextObj,'then').yields(result);
    sinon.stub(user,'update').yields("Carl",result);
    profilejs.updateProfile(req, res);
    resSpy.withArgs("/profile/Carl").calledOnce.should.be.true;
    req.assert.restore();
    req.getValidationResult.restore();
    nextObj.then.restore();
    user.update.restore();
  });
});
