var chai = require('chai');
var assert = chai.assert;
var should = chai.should();
var sinon = require('sinon');

var interestjs = require('../../models/interest');
var db = require('../../db');

describe('models/interest.js', function(){
  var details, profile, callback, callbackSpy, obj, toArray;
  beforeEach(function(){
    obj = {
      toArray : function(err, items){

      }
    }
    details = {
      find : function(param){
        return toArray;
      }
    };
    profile = {
      collection : function(myDb){
        return details;
      }
    };
    callbackSpy = sinon.spy(callback);
  });

  afterEach(function () {
    db.get.restore();
    obj.toArray.restore();
  });

  it('should get the user with interests', function(){
    sinon.stub(db,'get').returns(profile);
    sinon.stub(details,'find').returns(obj);
    var myStub = sinon.stub(obj,'toArray').yields(3,"Carl");
    interestjs.getAll(callbackSpy);
    callbackSpy.withArgs(3,"Carl").calledOnce.should.be.true;
    details.find.restore();
  });
});
