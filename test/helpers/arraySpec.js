var chai = require('chai');
var assert = chai.assert;
var expect = chai.expect;
var should = chai.should();
var sinon = require('sinon');

var arrayjs = require('../../helpers/array');

describe('helpers/array.js', function(){
  it('should return an object if that object is an array', function(){
    var Array = [1,2,3];
    var value = arrayjs.toArray(Array);
    assert.equal(value,Array);
  });

  it('should return an array of the object if an object is not an array', function(){
    var Array = 2;
    var value = arrayjs.toArray(Array);
    expect(value).to.have.members([Array]);
  });

  it('should return null if an object is nothing', function(){
    var Array = null;
    var value = arrayjs.toArray(Array);
    assert.equal(value,null);
  });
});
