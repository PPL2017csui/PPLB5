var MongoClient = require('mongodb').MongoClient

// mongodb state
var state = {
  db: null,
}

// connect to mongodb
/*istanbul ignore next */
exports.connect = function(url, done) {

  /* istanbul ignore if else */
    if (state.db) return done()

    MongoClient.connect(url, function(err, db) {
        /* istanbul ignore if else */

    if (err) return done(err)
        state.db = db
        done()
    })
}

// get mongodb database
/*istanbul ignore next */
exports.get = function() {
    return state.db
}

// close connection
/*istanbul ignore next */
exports.close = function(done) {
    /*istanbul ignore next */
    if (state.db) {
      /*istanbul ignore next */
        state.db.close(function(err, result) {
            state.db = null
            state.mode = null
            done(err)
        })
    }
}
